package com.skou.api.support;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * 自定义接口 继承JpaRepository,让我们具备了JpaRepository所提供的方法;<br>
 * 继承JpaSpecificationExecutor,让我们具备了Specification所提供分页能力。
 * 
 * @author ds
 * @param <T>
 * @param <ID>
 */
@NoRepositoryBean
public interface CustomRepository<T, ID extends Serializable>
		extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {

}
