package com.skou.api.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *基础信息表
 * @author yudeshan
 *
 */
@Entity
public class Location {

	/**
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 标签Mac
	 */
	private String tagMac;
	
	/**
	 * 所属门店ID
	 */
	private Long buildingId;
	
	/**
	 *所在地区地图ID 
	 */
	private Long mapId;
	
	/**
	 *所在地区ID 
	 */
	private Long areaId;
	
	/**
	 * Ap X坐标
	 */
	private Long gisX;
	
	/**
	 * Ap Y坐标
	 */
	private Long gisY;
	
	/**
	 * 时间
	 */
	private Date startTime;
	
	/**
	 * 是否在线
	 * 0:在线<br>
	 * 1:离线<br>
	 */
	private String isOnline;
	
	/**
	 * 创建时间
	 */
	private Date createTime;
	
	/**
	 * 状态<br>
	 * 1:在线有位置<br>
	 * 2:仅扫描无位置<br>
	 * 3:离线<br>
	 */
	private Long statu;
	
	/**
	 * 下线原因<br>
	 * 1:正常下线<br>
	 * 2:系统下线<br>
	 * 3:定期轮询获取<br>
	 * 4:维护系统辅助
	 */
	private Long offlineReason;
	
	/**
	 * Ap的Mac地址
	 */
	private String apMac;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTagMac() {
		return tagMac;
	}

	public void setTagMac(String tagMac) {
		this.tagMac = tagMac;
	}

	public Long getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(Long buildingId) {
		this.buildingId = buildingId;
	}

	public Long getMapId() {
		return mapId;
	}

	public void setMapId(Long mapId) {
		this.mapId = mapId;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public Long getGisX() {
		return gisX;
	}

	public void setGisX(Long gisX) {
		this.gisX = gisX;
	}

	public Long getGisY() {
		return gisY;
	}

	public void setGisY(Long gisY) {
		this.gisY = gisY;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getIsOnline() {
		return isOnline;
	}

	public void setIsOnline(String isOnline) {
		this.isOnline = isOnline;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getStatu() {
		return statu;
	}

	public void setStatu(Long statu) {
		this.statu = statu;
	}

	public Long getOfflineReason() {
		return offlineReason;
	}

	public void setOfflineReason(Long offlineReason) {
		this.offlineReason = offlineReason;
	}

	public String getApMac() {
		return apMac;
	}

	public void setApMac(String apMac) {
		this.apMac = apMac;
	}
	
	
	
}
