package com.skou.api.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 停留时间基础表
 * @author yudeshan
 *
 */
@Entity
public class CountStayTime {

	
	/**
	 * 主键，自增
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 标签Mac
	 */
	private String tagMac;
	
	/**
	 * 所属门店ID
	 */	
	private Long buildingId;
	
	/**
	 * 所在地图ID
	 */	
	private Long mapId;
	
	/**
	 * 所在区域ID
	 */	
	private  Long areaId;
	
	/**
	 * 开始时间
	 */	
	private Date beginTime;
	
	/**
	 * 结束时间
	 */	
	private Date endTime;
	
	/**
	 * 是否统计完成<br>
	 * 0:未完成<br>
	 * 1:完成
	 */	
	private Long isCountOk;
	
	/**
	 * 是否离线<br>
	 * 0:在线<br>
	 * 1:离线
	 */	
	private Long isOffline;
	
	/**
	 * 创建时间
	 */	
	private Date createTime;
	
	/**
	 * 状态<br>
	 * 1:在线有位置<br>
	 * 2:仅扫描无位置<br>
	 * 3:离线
	 */	
	private Long statu;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTagMac() {
		return tagMac;
	}

	public void setTagMac(String tagMac) {
		this.tagMac = tagMac;
	}

	public Long getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(Long buildingId) {
		this.buildingId = buildingId;
	}

	public Long getMapId() {
		return mapId;
	}

	public void setMapId(Long mapId) {
		this.mapId = mapId;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Long getIsCountOk() {
		return isCountOk;
	}

	public void setIsCountOk(Long isCountOk) {
		this.isCountOk = isCountOk;
	}

	public Long getIsOffline() {
		return isOffline;
	}

	public void setIsOffline(Long isOffline) {
		this.isOffline = isOffline;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getStatu() {
		return statu;
	}

	public void setStatu(Long statu) {
		this.statu = statu;
	}
	
	
	
}
