package com.skou.api.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 文件操作工具
 * 
 * @author 余德山
 *
 */
public class FileUtils {

	/**
	 * 遍历文件夹下所有文件
	 * @param strPath
	 * @return
	 */
	public static List<File> getFileList(String strPath) {
		List<File> filelist = new ArrayList<File>();
		File dir = new File(strPath);
		File[] files = dir.listFiles(); // 该文件目录下文件全部放入数组
		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				String fileName = files[i].getName();
				if (files[i].isDirectory()) { // 判断是文件还是文件夹
					getFileList(files[i].getAbsolutePath()); // 获取文件绝对路径
				} else if (fileName.endsWith("csv") && files[i].length()>0) { // 判断文件名是否以.avi结尾
					filelist.add(files[i]);
				} else {
					continue;
				}
			}

		}
		return filelist;
	}

}
