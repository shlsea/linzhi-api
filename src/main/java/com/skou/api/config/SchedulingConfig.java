package com.skou.api.config;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.skou.api.service.CountStayTimeRepository;
import com.skou.api.service.LocationRepository;

/**
 * 同步定时器
 * 
 * @项目名称 同步API
 * @作者 余德山
 * @包名称 com.skou.api.utils.MyZipCompressing
 * @功能描述 TODO
 * @日期 2017年3月13日
 * @版本 V1.0.0
 */
@Configuration
@EnableScheduling
public class SchedulingConfig {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	

	
	
	@Autowired
	private LocationRepository locationRepository;
	@Autowired
	private CountStayTimeRepository countStayTimeRepository;

	/**
	 * 每五分钟定时同步一次示范区及地区名单
	 */
	// @Scheduled(cron = "0/5 * * * * ?") // 每5分钟执行一次
	// @Scheduled(cron = "0 15 12 * * ?") // 每天12点15分执行一次
	public void scheduler() {

		logger.info("定时任务");
	}

	@Scheduled(cron = "* 0/5 * * * ?")
	public void getLocation() throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println(df.format(new Date())+ ":基础信息表定时器执行");
		SimpleDateFormat yf = new SimpleDateFormat("yyyy-MM-dd");
		Date date=new Date();
		String today=yf.format(date);
		Long timer=date.getTime()-24*60*60*1000;
		Date yerter=new Date(timer);
		String yesterday=yf.format(yerter);
		System.out.println(today+":"+yesterday);
		
		
/*		MyZipCompressing.unzip("/Users/yudeshan/Downloads/api.zip", "/Users/yudeshan/Downloads/api");
		List<File> fileList = FileUtils.getFileList("/Users/yudeshan/Downloads/api");*/

		
	}

	@Scheduled(cron = "5 * * * * ?")
	public void getCountStayTime() throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		logger.info(df.format(new Date())+ "停留时间表定时器执行");
		SimpleDateFormat yf = new SimpleDateFormat("yyyy-MM-dd");
		Date date=new Date();
		String today=yf.format(date);
		Long timer=date.getTime()-24*60*60*1000;
		Date yerter=new Date(timer);
		String yesterday=yf.format(yerter);

		
	}
	

}
