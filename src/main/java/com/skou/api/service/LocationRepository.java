package com.skou.api.service;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.skou.api.pojo.Location;
import com.skou.api.support.CustomRepository;

@Service
public interface LocationRepository extends CustomRepository<Location, Long>{

	Location findByTagMacAndCreateTime(String tagMac, Date createTime);

	Location findByTagMacAndStartTime(String tagMac, Date createTime);

}
