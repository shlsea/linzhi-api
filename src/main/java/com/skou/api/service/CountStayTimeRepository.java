package com.skou.api.service;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.skou.api.pojo.CountStayTime;
import com.skou.api.support.CustomRepository;

@Service
public interface CountStayTimeRepository extends CustomRepository<CountStayTime, Long>{

	CountStayTime findByTagMacAndBeginTimeAndEndTimeAndIsCountOk(String tagMac, Date beginTime, Date endTime,
			Long isCountOk);

}
